output "dockerhost_external_ip" {
  value = "${module.dockerhost.dockerhost_external_ip}"
}

output "dockerhost_internal_ip" {
  value = "${module.dockerhost.dockerhost_internal_ip}"
}
