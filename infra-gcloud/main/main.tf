provider "google" {
  version = "1.4.0"
  project = "${var.project}"
  region  = "${var.region}"
}

module "dockerhost" {
  source           = "../modules/dockerhost"
  public_key_path  = "${var.public_key_path}"
  private_key_path = "${var.private_key_path}"
  ssh_user         = "${var.ssh_user}"
  zone             = "${var.zone}"
}

module "vpc" {
  source = "../modules/vpc"
}

terraform {
  required_version = ">= 0.11"

  backend "gcs" {
    bucket = "docker-tfstate-199715"
  }
}
