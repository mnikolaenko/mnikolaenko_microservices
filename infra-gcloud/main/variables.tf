variable "project" {
  description = "Project ID"
}

variable "region" {
  description = "Project region"
  default     = "europe-west1"
}

variable "zone" {
  description = "Zone"
  default     = "europe-west1-b"
}

variable "private_key_path" {
  description = "Path to private ssh key"
}

variable "public_key_path" {
  description = "Path to public ssh key"
}

variable "ssh_user" {
  description = "Name of ssh user"
}
