provider "google" {
  version = "1.4.0"
  project = "${var.project}"
  region  = "${var.region}"
}

module "vpc" {
  source = "../modules/vpc"
}

terraform {
  required_version = ">= 0.11"

  backend "gcs" {
    bucket = "docker-tfstate-199715"
    prefix = "vpc-docker-host"
  }
}
