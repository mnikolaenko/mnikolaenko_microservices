variable "project" {
  description = "Project ID"
}

variable "region" {
  description = "Project region"
  default     = "europe-west1"
}

variable "zone" {
  description = "Zone"
  default     = "europe-west1-b"
}
