variable "dockerhost_disk_image" {
  description = "Disk image"
  default     = "ubuntu-1804-lts"
}

variable "private_key_path" {
  description = "Path to private ssh key"
}

variable "public_key_path" {
  description = "Path to public ssh key"
}

variable "ssh_user" {
  description = "Name of ssh user"
}

variable "zone" {
  description = ""
}
