resource "google_compute_instance" "dockerhost" {
  name         = "dockerhost"
  machine_type = "g1-small"
  zone         = "${var.zone}"

  boot_disk {
    initialize_params {
      image = "${var.dockerhost_disk_image}"
    }
  }

  network_interface {
    network = "default"

    access_config = {
      nat_ip = "${google_compute_address.dockerhost_ip.address}"
    }
  }

  metadata {
    ssh-keys = "${var.ssh_user}:${file(var.public_key_path)}"
  }

  connection {
    type        = "ssh"
    user        = "${var.ssh_user}"
    agent       = false
    private_key = "${var.private_key_path}"
  }
}

resource "google_compute_address" "dockerhost_ip" {
  name = "dockerhost-ip"
}
